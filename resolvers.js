// import {AuthenticationError} from 'apollo-server';
import { ObjectId } from 'promised-mongo';

const { AuthenticationError, ForbiddenError } = require('apollo-server');

const { query } = require('nact');

const resource = 'meeting' || 'event';

module.exports = {

  Mutation: {
    changeEvent: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      if (args._id) {
        return await query(collectionItemActor, { type: 'event', search: { _id: args._id }, input: args.input }, global.actor_timeout);
      }
      return await query(collectionItemActor, { type: 'event', input: args.input }, global.actor_timeout);
    },

    changeMeeting: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      args.input.team_id = new ObjectId(args.input.team_id);

      if (args._id) {
        return await query(collectionItemActor, { type: 'meeting', search: { _id: args._id }, input: args.input }, global.actor_timeout);
      }
      return await query(collectionItemActor, { type: 'meeting', input: args.input }, global.actor_timeout);
    },
  },
  Query: {

    getEvent: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      return await query(collectionItemActor, { type: 'event', search: { _id: args._id } }, global.actor_timeout);
    },

    getEvents: async (obj, args, ctx, info) => {
      const collectionActor = ctx.children.get('collection');

      return await query(collectionActor, { type: 'event' }, global.actor_timeout);
    },
    getMeeting: async (obj, args, ctx, info) => {
      let meetings = await ctx.db.meeting.aggregate(
        [
          { $match: { _id: new ObjectId(args.id) } },
          {
            $lookup: {
              from: 'team',
              localField: 'team_id',
              foreignField: '_id',
              as: 'team',
            },
          },
          { $unwind: '$team' },
          { $limit: 1 },
        ],
      );

      let meeting = meetings[0];
      return meeting || {};
    },

    getMeetings: async (obj, args, ctx, info) => {
      let user = await ctx.user;

      if (!user) throw new AuthenticationError('must authenticate');

      if (!ctx.access.can(user.role).readAny(resource).granted) throw new ForbiddenError('Forbidden');

      return await ctx.db.meeting.aggregate(
        [
          { $sort: { date: -1 } },
          {
            $lookup: {
              from: 'team',
              localField: 'team_id',
              foreignField: '_id',
              as: 'team',
            },
          },
          { $unwind: '$team' },
        ],
      );
    },
  },
};
